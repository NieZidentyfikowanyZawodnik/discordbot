const {Client,Intents} = require('discord.js')
const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS]});
const { Routes } = require('discord-api-types/v9');
const fs = require('fs');
const { REST } = require('@discordjs/rest');

const clientId = '944329936288751657'
const commandDefinitions = [];
const commands = [];
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	commandDefinitions.push(command.data.toJSON());    
    commands.push(command)
}

client.on('ready', async () => {
    const guilds = client.guilds.cache;
    const rest = new REST({ version: '9' }).setToken('OTQ0MzI5OTM2Mjg4NzUxNjU3.YhAByQ.njmU6uDzAth8YCXtKE3lkDyIZvs');

    await (async () => {
        try {
            console.log('Started refreshing application (/) commands.');
            await guilds.forEach(async guild => {
                await rest.put(
                    Routes.applicationGuildCommands(clientId, guild.id),
                    { body: commandDefinitions },
                );
                console.log(`Server ${guild.name} updated!`)
            })
            console.log('Successfully reloaded application (/) commands.');
        } catch (error) {
            console.error(error);
        }
    })();
    console.log(`bot is ready to go '${client.user.client.user.username}'`)
})

client.on('interactionCreate', async interaction => {
    console.log(interaction)
    if (!interaction.isCommand()) return;

	const command = commands.find(c => c.data.name == interaction.commandName);

	if (!command) return;

	try {
		await command.execute(interaction);
	} catch (error) {
		console.error(error);
		await interaction.reply({ content: 'There was an error while executing this command!', ephemeral: true });
	}
});
client.login('OTQ0MzI5OTM2Mjg4NzUxNjU3.YhAByQ.njmU6uDzAth8YCXtKE3lkDyIZvs')